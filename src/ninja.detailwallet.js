(function (wallets, qrCode, privateKey, translator) {
	var detail = wallets.detailwallet = {
		isOpen: function () {
			return (document.getElementById("detailwallet").className.indexOf("selected") != -1);
		},

		open: function () {
			document.getElementById("detailarea").style.display = "block";
			document.getElementById("detailprivkey").focus();
		},

		close: function () {
			document.getElementById("detailarea").style.display = "none";
		},

		openCloseFaq: function (faqNum) {
			// do close
			if (document.getElementById("detaila" + faqNum).style.display == "block") {
				document.getElementById("detaila" + faqNum).style.display = "none";
				document.getElementById("detaile" + faqNum).setAttribute("class", "more");
			}
			// do open
			else {
				document.getElementById("detaila" + faqNum).style.display = "block";
				document.getElementById("detaile" + faqNum).setAttribute("class", "less");
			}
		},

		getKeyFromInput: function () {
			var key = document.getElementById("detailprivkey").value.toString().replace(/^\s+|\s+$/g, ""); // trim white space
			document.getElementById("detailprivkey").value = key;
			return key;
		},

		checkAndShowMini: function (key) {
			if (Gdrcoin.ECKey.isMiniFormat(key)) {
				// show Private Key Mini Format
				document.getElementById("detailprivmini").innerHTML = key;
				document.getElementById("detailmini").style.display = "block";
			}
		},

		checkAndShowBase6: function (key) {
			if (Gdrcoin.ECKey.isBase6Format(key)) {
				// show Private Key Base6 Format
				document.getElementById("detailprivb6").innerHTML = key;
				document.getElementById("detailb6").style.display = "block";
			}
		},

		keyToECKeyWithBrain: function (key) {
			var markKey = new Gdrcoin.ECKey(key);
			if (markKey.error != null) {
				alert(translator.get("detailalertnotvalidprivatekey") + "\n" + markKey.error);
			}
			else if (markKey.priv == null) {
				// enforce a minimum passphrase length
				if (key.length >= wallets.brainwallet.minPassphraseLength) {
					// Deterministic Wallet confirm box to ask if user wants to SHA256 the input to get a private key
					var usePassphrase = confirm(translator.get("detailconfirmsha256"));
					if (usePassphrase) {
						var bytes = Crypto.SHA256(key, { asBytes: true });
						markKey = new Gdrcoin.ECKey(bytes);
					}
				}
				else {
					alert(translator.get("detailalertnotvalidprivatekey"));
				}
			}
			return markKey;
		},

		decryptBip38: function () {
			detail.clear();
			var key = detail.getKeyFromInput();
			if (key == "") {
				return;
			}
			if (privateKey.isBIP38Format(key) == false) {
				return;
			}
			document.getElementById("detailbip38toggle").style.display = "none";
			var passphrase = document.getElementById("detailprivkeypassphrase").value.toString()
			if (passphrase == "") {
				alert(translator.get("bip38alertpassphraserequired"));
				return;
			}
			document.getElementById("busyblock").className = "busy";
			// show Private Key BIP38 Format
			document.getElementById("detailprivbip38").innerHTML = key;
			document.getElementById("detailbip38").style.display = "block";
			qrCode.showQrCode({
				"detailqrcodeprivatebip38": key
			}, 4);
			privateKey.BIP38EncryptedKeyToByteArrayAsync(key, passphrase, function (markKeyOrError) {
				document.getElementById("busyblock").className = "";
				if (markKeyOrError.message) {
					alert(markKeyOrError.message);
					detail.clear();
				} else {
					detail.populateKeyDetails(new Gdrcoin.ECKey(markKeyOrError));
				}
			});
		},

		encryptBip38: function () {
			detail.clear();
			var key = detail.getKeyFromInput();
			if (key == "") {
				return;
			}
			if (privateKey.isBIP38Format(key)) {
				return;
			}
			detail.checkAndShowMini(key);
			detail.checkAndShowBase6(key);
			var markKey = detail.keyToECKeyWithBrain(key);
			if (markKey.priv == null) {
				return;
			}
			var detailEncryptCheckbox = document.getElementById("detailbip38checkbox");
			if (detailEncryptCheckbox.checked == true) {
				document.getElementById("detailbip38commands").style.display = "block";
				var passphrase = document.getElementById("detailprivkeypassphrase").value.toString()
				if (passphrase == "") {
					alert(translator.get("bip38alertpassphraserequired"));
					return;
				}
				document.getElementById("busyblock").className = "busy";
				privateKey.BIP38PrivateKeyToEncryptedKeyAsync(markKey.getGdrcoinWalletImportFormat(), passphrase, markKey.compressed, function (encryptedKey) {
					qrCode.showQrCode({
						"detailqrcodeprivatebip38": encryptedKey
					}, 4);
					// show Private Key BIP38 Format
					document.getElementById("detailprivbip38").innerHTML = encryptedKey;
					document.getElementById("detailbip38").style.display = "block";
					document.getElementById("busyblock").className = "";
				});
				detail.populateKeyDetails(markKey);
			}
		},

		viewDetails: function () {
			detail.clear();
			document.getElementById("detailbip38checkbox").checked = false;
			var key = detail.getKeyFromInput();
			if (key == "") {
				return;
			}
			if (privateKey.isBIP38Format(key)) {
				document.getElementById("detailbip38commands").style.display = "block";
				document.getElementById("detailprivkeypassphrase").focus();
				return;
			}
			document.getElementById("detailbip38commands").style.display = "none";
			detail.checkAndShowMini(key);
			detail.checkAndShowBase6(key);
			var markKey = detail.keyToECKeyWithBrain(key);
			if(markKey.priv == null){
				return;
			}
			detail.populateKeyDetails(markKey);
		},

		populateKeyDetails: function (markKey) {
			if (markKey.priv != null) {
				// get the original compression value and set it back later in this function
				var originalCompression = markKey.compressed;
				markKey.setCompressed(false);
				document.getElementById("detailprivhex").innerHTML = markKey.toString().toUpperCase();
				document.getElementById("detailprivb64").innerHTML = markKey.toString("base64");
				var gdrcoinAddress = markKey.getGdrcoinAddress();
				var wif = markKey.getGdrcoinWalletImportFormat();
				document.getElementById("detailpubkey").innerHTML = markKey.getPubKeyHex();
				document.getElementById("detailaddress").innerHTML = gdrcoinAddress;
				document.getElementById("detailprivwif").innerHTML = wif;
				markKey.setCompressed(true);
				var gdrcoinAddressComp = markKey.getGdrcoinAddress();
				var wifComp = markKey.getGdrcoinWalletImportFormat();
				document.getElementById("detailpubkeycomp").innerHTML = markKey.getPubKeyHex();
				document.getElementById("detailaddresscomp").innerHTML = gdrcoinAddressComp;
				document.getElementById("detailprivwifcomp").innerHTML = wifComp;
				markKey.setCompressed(originalCompression); // to satisfy the key pool
				var pool1 = new Gdrcoin.ECKey(wif); // to satisfy the key pool
				var pool2 = new Gdrcoin.ECKey(wifComp); // to satisfy the key pool

				qrCode.showQrCode({
					"detailqrcodepublic": gdrcoinAddress,
					"detailqrcodepubliccomp": gdrcoinAddressComp,
					"detailqrcodeprivate": wif,
					"detailqrcodeprivatecomp": wifComp
				}, 4);
			}
		},

		clear: function () {
			var key = detail.getKeyFromInput();
			if (privateKey.isBIP38Format(key)) {
				document.getElementById("detailbip38commands").style.display = "block";
				document.getElementById("detailbip38toggle").style.display = "none";
				document.getElementById("detailbip38decryptspan").style.display = "inline-block";
				document.getElementById("detailbip38encryptspan").style.display = "none";
				document.getElementById("detailbip38checkbox").checked = false;
			}
			else {
				document.getElementById("detailbip38toggle").style.display = "block";
				if (document.getElementById("detailbip38checkbox").checked) {
					document.getElementById("detailbip38commands").style.display = "block";
					document.getElementById("detailbip38decryptspan").style.display = "none";
					document.getElementById("detailbip38encryptspan").style.display = "inline-block";
				}
				else {
					document.getElementById("detailbip38commands").style.display = "none";
					document.getElementById("detailbip38decryptspan").style.display = "inline-block";
					document.getElementById("detailbip38encryptspan").style.display = "none";
				}
			}
			document.getElementById("detailpubkey").innerHTML = "";
			document.getElementById("detailpubkeycomp").innerHTML = "";
			document.getElementById("detailaddress").innerHTML = "";
			document.getElementById("detailaddresscomp").innerHTML = "";
			document.getElementById("detailprivwif").innerHTML = "";
			document.getElementById("detailprivwifcomp").innerHTML = "";
			document.getElementById("detailprivhex").innerHTML = "";
			document.getElementById("detailprivb64").innerHTML = "";
			document.getElementById("detailprivb6").innerHTML = "";
			document.getElementById("detailprivmini").innerHTML = "";
			document.getElementById("detailprivbip38").innerHTML = "";
			document.getElementById("detailqrcodepublic").innerHTML = "";
			document.getElementById("detailqrcodepubliccomp").innerHTML = "";
			document.getElementById("detailqrcodeprivate").innerHTML = "";
			document.getElementById("detailqrcodeprivatecomp").innerHTML = "";
			document.getElementById("detailb6").style.display = "none";
			document.getElementById("detailmini").style.display = "none";
			document.getElementById("detailbip38").style.display = "none";
		},

		enterOnPassphrase: function () {
			var detailEncryptCheckbox = document.getElementById("detailbip38checkbox");
			if (detailEncryptCheckbox.checked) {
				detail.encryptBip38();
			}
			else {
				detail.decryptBip38();
			}
		},

		toggleEncrypt: function (element) {
			// enable/disable passphrase textbox
			var bip38CommandDisplay = document.getElementById("detailbip38commands").style.display;
			var key = detail.getKeyFromInput();

			if (element.checked == true) {
				if (privateKey.isBIP38Format(key)) {
					document.getElementById("detailbip38toggle").style.display = "none";
					document.getElementById("detailbip38commands").style.display = "block";
					document.getElementById("detailprivkeypassphrase").focus();
					return;
				}
				else {
					// show encrypt button
					document.getElementById("detailbip38commands").style.display = "block";
					document.getElementById("detailprivkeypassphrase").focus();
					document.getElementById("detailbip38decryptspan").style.display = "none";
					document.getElementById("detailbip38encryptspan").style.display = "inline-block";
				}
			}
			else {
				// show decrypt button
				document.getElementById("detailbip38decryptspan").style.display = "inline-block";
				document.getElementById("detailbip38encryptspan").style.display = "none";
				document.getElementById("detailbip38commands").style.display = "none";
			}
		}
	};
})(ninja.wallets, ninja.qrCode, ninja.privateKey, ninja.translator);
