(function (wallets, qrCode) {
	var single = wallets.singlewallet = {
		isOpen: function () {
			return (document.getElementById("singlewallet").className.indexOf("selected") != -1);
		},

		open: function () {
			if (document.getElementById("markaddress").innerHTML == "") {
				single.generateNewAddressAndKey();
			}
			document.getElementById("singlearea").style.display = "block";
		},

		close: function () {
			document.getElementById("singlearea").style.display = "none";
		},

		// generate gdrcoin address and private key and update information in the HTML
		generateNewAddressAndKey: function () {
			try {
				var key = new Gdrcoin.ECKey(false);
				key.setCompressed(true);
				var gdrcoinAddress = key.getGdrcoinAddress();
				var privateKeyWif = key.getGdrcoinWalletImportFormat();
				document.getElementById("markaddress").innerHTML = gdrcoinAddress;
				document.getElementById("markprivwif").innerHTML = privateKeyWif;
				var keyValuePair = {
					"qrcode_public": gdrcoinAddress,
					"qrcode_private": privateKeyWif
				};
				qrCode.showQrCode(keyValuePair, 4);
			}
			catch (e) {
				// browser does not have sufficient JavaScript support to generate a gdrcoin address
				alert(e);
				document.getElementById("markaddress").innerHTML = "error";
				document.getElementById("markprivwif").innerHTML = "error";
				document.getElementById("qrcode_public").innerHTML = "";
				document.getElementById("qrcode_private").innerHTML = "";
			}
		}
	};
})(ninja.wallets, ninja.qrCode);
